# MARS Telegraf Sidecar #

Note: The telegraf sidecar is used for getting metrics of the java application that are running on tomcat. If your application is not running on tomcat please check the telelegraf service first.

To learn more on the difference between telegraf service and sidecar check this [page](https://etsinternal.atlassian.net/wiki/spaces/TA/pages/2008154319/How+to+use+the+Telegraf+Image#Types-of-telegraf-service).

The telegraf sidecar image is deployed as a secondary task on a tomcat application job. The telegraf sidecar container will be deployed on the same node as the tomcat application container. This solution was created due to the lack of the tagging capability of jolokia. This way metrics tagging will be handled by telegraf application itself. 

### How do I get set up? ###

* Telegraf sidecar is deployed along side of the tomcat application that you want to get metrics from.
* To learn more on how to use the telegraf sidecar check this [page](https://etsinternal.atlassian.net/wiki/spaces/TA/pages/1986200191/Exposing+Metrics+for+a+Tomcat+Nomad+Job).

### Building and pushing the image to the repository ###

```docker build -t etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-sidecar:<version> .```

```docker push etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-sidecar:<version>```

### Who do I talk to? ###

* DevOps Team