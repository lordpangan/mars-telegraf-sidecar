job "tomcatsample" {
  
    # Declare which datacenter to deploy the job
    datacenters = ["dc1"]
    
    group "tomcatsample" {

        network {
            # Setting the job network to bridge
            mode = "bridge"
            
            # Maps in-comming traffic to correct ports
            port "tomcathttp" {
                to=8080
            }
        }
        
        # Register telegraf to consul
        service {
            name = "tomcatsample"
            port = "tomcathttp"

            tags = [
                "urlprefix-tomcatsample.dev.marsfi.com/"
            ]

            check {
                type = "http"
                path = "/sample/hello"
                interval = "2s"
                timeout = "2s"
            }
        }

        # main tomcat app task
        task "tomcatsample"{
        
            # Declares if your job will use docker or binary file etc.
            driver = "docker"

            config{
                # Declares credentials to use when pulling container from artifactory
                auth = {
                    server_address = "etradingsoftware-marsfi-docker-local.jfrog.io"
                    username = "ansible"
                    password = "spring1"
                }
                
                # Docker image to use
                image = "etradingsoftware-marsfi-docker-local.jfrog.io/tomcatsample:latest"
                
                 # Mount ca certs
                volumes = [
                    "/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem:/etc/ssl/certs/ca-certificates.crt",
                    "/etc/pki/ca-trust/extracted/java/cacerts:/etc/ssl/certs/java/cacerts"
                ]
            }
        }

        # sidecar telegraf app task
        task "telegraf"{
        
            # Declares if your job will use docker or binary file etc.
            driver = "docker"
            
            
            env {
                # metrics tags variables
                # set the current alloc id
                ALLOC_NAME = "${NOMAD_ALLOC_ID}"
                
                # set the current node unique name
                NODE_HOSTNAME = "${node.unique.name}"
                APP_NAME = "tomcatsample"
                
                # influxdb variables
                ENV INFLUXDB_URL = "default.localhost"
                ENV INFLUXDB_USERNAME = "telegraf"
                ENV INFLUXDB_PASSWORD = "metricsmetricsmetrics"
                
                # jolokia variables
                ENV JOLOKIA_APP_URL = "default.localhost"
                ENV JOLOKIA_APP_USERNAME = "jolokia"
                ENV JOLOKIA_APP_PASSWORD = "jolokia"
                
                # telegraf scraping interval
                ENV FLUSH_INTERVAL = "10s"              
            }

            config{
                # Declares credentials to use when pulling container from artifactory
                auth = {
                    server_address = "etradingsoftware-marsfi-docker-local.jfrog.io"
                    username = "ansible"
                    password = "spring1"
                }
                
                 # Docker image to use
                image = "etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-sidecar:latest"
            }
        }
    }
}